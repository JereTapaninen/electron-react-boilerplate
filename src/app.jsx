import React, {PureComponent} from "react";

class App extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>Hello world!</div>
        );
    }
}

export default App;
