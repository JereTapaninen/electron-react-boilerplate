import React from "react";
import {shallow} from "enzyme";
import {expect} from "chai";
import App from "../src/app";

let wrapper;

describe("App", function() {
    it("should mount", function() {
        wrapper = shallow(<App />);
        expect(wrapper.exists()).equals(true);
    });
});