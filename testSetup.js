process.env.NODE_ENV = "test";

var Adapter = require("enzyme-adapter-react-16");
var Enzyme = require("enzyme");
Enzyme.configure({adapter: new Adapter()});

require("babel-register")();

require.extensions[".css"] = function() { return null; };

var jsdom = require("jsdom");
const {JSDOM} = jsdom;
var exposedProperties = ["window", "navigator", "document"];

const {document} = (new JSDOM("")).window;
global.document = document;
global.navigator = {userAgent: "node.js"};
global.window = document.defaultView;

Object.keys(document.defaultView).forEach(function(property) {
    if (typeof global[property] === "undefined") {
        exposedProperties.push(property);
        global[property] = document.defaultView[property];
    }
});
