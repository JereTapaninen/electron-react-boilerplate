# electron-react-boilerplate

This is a minimal setup boilerplate for Electron + React + UT (Mocha, Chai, Enzyme, JSDOM).

<b>But what about Express?</b>

You don't need Express with Electron. You don't even need a REST server with Electron. You can call file system operations from all around the application, be it the main.js file or some reactComponent.jsx file. That is the point of Electron.

<b>To use:</b> <br />
1. Clone this repository: `git@gitlab.com:JereTapaninen/electron-react-boilerplate.git`
2. Run `npm install`
3. Start programming! Run the program with `npm start`.
4. Run Unit Tests with `npm test` or `npm run test`.
    * Eslint will run with npm test, but you can test it separately with command `npm run eslint`.
